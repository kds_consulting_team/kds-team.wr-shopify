import json
import logging
from csv2json.hone_csv2json import Csv2JsonConverter
from typing import List, Dict, Optional, Generator


class JsonConverter:

    def __init__(self, nesting_delimiter: str = '__',
                 chunk_size: Optional[int] = 1,
                 infer_data_types=True,
                 column_data_types: Optional[List[Dict[str, str]]] = None,
                 column_name_override: Optional[dict] = None,
                 data_wrapper: Optional[str] = None):
        self.chunk_size = chunk_size
        self.nesting_delimiter = nesting_delimiter
        self.infer_data_types = infer_data_types
        self.column_data_types = column_data_types or []
        self.data_wrapper = data_wrapper
        self.column_name_override = column_name_override or {}

    def _transform(self, data: dict):
        return data

    def convert_stream(self, reader, source_id_column: str = None,
                       pop_source_column=False) -> Generator[dict, None, None]:
        header = next(reader, None)
        converter = Csv2JsonConverter(header, delimiter=self.nesting_delimiter)
        # fetch first row
        row = next(reader, None)

        if not row:
            logging.warning('The file is empty!')

        while row:  # outer loop, create chunks
            continue_it = True
            i = 0
            json_string = '[' if self.chunk_size > 1 else ''
            while continue_it:
                i += 1
                result = converter.convert_row(row=row,
                                               coltypes=self.column_data_types,
                                               delimit=self.nesting_delimiter,
                                               colname_override=self.column_name_override,
                                               infer_undefined=self.infer_data_types)

                json_string += json.dumps(result[0])
                row = next(reader, None)

                if not row or (self.chunk_size and i >= self.chunk_size):
                    continue_it = False

                if continue_it:
                    json_string += ','

            json_string += ']' if self.chunk_size > 1 else ''
            data = json.loads(json_string)
            data = self._transform(data)
            source_id = ''
            # remove source column but keep the id column
            if source_id_column and pop_source_column:
                source_id = data.pop(source_id_column, '')
            elif not pop_source_column:
                source_id = data.get(source_id_column, '')

            yield data, source_id


class ProductConverter(JsonConverter):

    def __init__(self):
        super().__init__(chunk_size=1)
        # forced types
        self.column_data_types = [{"column": "variants", "type": "object"},
                                  {"column": "images", "type": "object"}]

    def _transform(self, data: dict):
        # transform options
        options_dict = data.get('options', {})
        options_object = []
        if options_dict:
            for o in options_dict:
                option = {"name": o,
                          "values": options_dict[o]}
                options_object.append(option)
            data['options'] = options_object
        return data


def get_object_converter(endpoint_type: str):
    if endpoint_type == 'product':
        return ProductConverter()
    if endpoint_type == 'product_image':
        return JsonConverter(column_data_types=[{"column": "variant_ids", "type": "object"}])
    else:
        return JsonConverter()
