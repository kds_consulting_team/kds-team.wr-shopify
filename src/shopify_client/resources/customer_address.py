from shopify.base import ShopifyResource


class Address(ShopifyResource):
    _prefix_source = "/customers/$customer_id/"

    @classmethod
    def _prefix(cls, options={}):
        customer_id = options.get("customer_id")
        if customer_id:
            return "%s/customers/%s" % (cls.site, customer_id)
        else:
            return cls.site

    def save(self):
        if "customer_id" not in self._prefix_options:
            self._prefix_options["customer_id"] = self.customer_id
        return super(ShopifyResource, self).save()
