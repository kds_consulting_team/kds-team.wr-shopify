'''
Template Component main class.

'''
import csv
import json
import logging
from datetime import datetime
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

# configuration variables
from json_converter import get_object_converter
from shopify_client.client import ShopifyClient, ShopifyWriteError

# configuration variables
KEY_POP_SRC_COL = 'remove_from_data'
KEY_MODE = 'mode'
KEY_SOURCE_ID_REF = 'source_id_reference'
KEY_SOURCE_ID = 'source_id_col'
RESULT_COLUMNS = ['timestamp', 'endpoint_type', 'mode', 'source_id', 'created_id', 'status',
                  'err_message']
KEY_API_TOKEN = '#api_token'

KEY_LOADING_OPTIONS = 'loading_options'

KEY_SHOP = 'shop'
KEY_ENDPOINT = 'endpoint'

# #### Keep for debug
KEY_DEBUG = 'debug'

# list of mandatory parameters => if some is missing, component will fail with readable message on initialization.
MANDATORY_PARS = [KEY_API_TOKEN, KEY_SHOP, KEY_ENDPOINT]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__()

        self.client: ShopifyClient = None

        if not self.configuration.parameters.get('debug'):
            logging.getLogger('pyactiveresource.connection').disabled = True
            logging.getLogger('backoff').disabled = True

    def _init_client(self):
        self.client = ShopifyClient(self.configuration.parameters[KEY_SHOP],
                                    self.configuration.parameters[KEY_API_TOKEN],
                                    api_version=self.configuration.parameters.get('api_version', '2022-10'))

    def run(self):
        '''
        Main execution code
        '''

        params = self.configuration.parameters
        self.validate_configuration_parameters(MANDATORY_PARS)
        self._init_client()

        if params[KEY_ENDPOINT] == 'inventory_level':
            params[KEY_MODE] = 'update'

        in_tables = self.get_input_tables_definitions()

        if len(in_tables) == 0:
            logging.exception('There is no table specified on the input mapping! You must provide one input table!')
            exit(1)
        elif len(in_tables) > 1:
            logging.warning(
                'There is more than one table specified on the input mapping! You must provide one input table!')

        in_table = in_tables[0]

        result_table = self.create_out_table_definition('results.csv', incremental=True,
                                                        primary_key=['timestamp', 'endpoint_type', 'created_id',
                                                                     'source_id'])
        counter = 0
        with open(in_table.full_path, 'r') as inp, open(result_table.full_path, 'w+') as out:
            converter = get_object_converter(params[KEY_ENDPOINT])
            reader = csv.reader(inp, lineterminator='\n')
            writer = csv.DictWriter(out, fieldnames=RESULT_COLUMNS)
            writer.writeheader()
            for obj, source_id in converter.convert_stream(reader, params[KEY_SOURCE_ID_REF][KEY_SOURCE_ID],
                                                           params[KEY_SOURCE_ID_REF][KEY_POP_SRC_COL]):
                result = {}
                error = ''
                try:
                    result = self.client.write_object(obj, params[KEY_ENDPOINT], params[KEY_MODE])
                except ShopifyWriteError as e:
                    logging.warning(
                        f'Write to {params[KEY_ENDPOINT]} with id {source_id} failed! See log detail for more info.',
                        extra={"full_message": e.error})
                    error = json.dumps(e.error)
                counter += 1
                if counter % 100 == 0:
                    logging.info(f'{counter} records processed.')

                writer.writerow(self._build_result_log(result.get('id', ''), source_id, params[KEY_MODE], error))

        self.write_manifest(result_table)

    def _build_result_log(self, created_id: str, source_id, mode, error_message='') -> dict:
        if error_message:
            status = 'failed'
        else:
            status = 'success'

        return {'timestamp': datetime.now(),
                'endpoint_type': self.configuration.parameters[KEY_ENDPOINT],
                'mode': mode,
                'source_id': source_id,
                'created_id': created_id,
                'status': status,
                'err_message': error_message}


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
