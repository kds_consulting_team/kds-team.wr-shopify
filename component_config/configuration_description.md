# Prerequisites

To enable this application, you need to:
 
- [enable custom app development](https://help.shopify.com/en/manual/apps/app-types/custom-apps) for your store. 
- Create a custom app and install it in your store.
- Enable `Write access` Admin API access scope for the following objects:
    - `Orders`
    - `Products`
    - `Inventory`
    - `Customers`
- Obtain the API access token under the API credentials tab of your app.
    
    
 
# Configuration

## Authentication 

Make sure you have created a custom application with appropriate permissions.

### Admin API access token

Admin API access token of your private app.

### Shop name

Your shop ID found in the URL, e.g., `[shop_id]`.myshopify.com


## Setup

- Each configuration row accepts a single table on the input. It will be then converted into a JSON model that is required by 
the specified endpoint.
- Each column in the table translates to a JSON property. In some cases, you may include a full JSON string as the column value. 
- Objects may be flattened into columns using the `__` separator. E.g., `customer__id` leads to `{"customer":{"id":"VAL"}}`


**NOTE:** It is possible to include one extra column that serves as a reference to your source data row and is then excluded from the payload.  
You can set this column by setting the `source_id` parameter.

Additional documentation is available [here](https://bitbucket.org/kds_consulting_team/kds-team.wr-shopify/src/master/README.md).