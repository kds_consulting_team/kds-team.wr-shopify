Create or update the following objects in your Shopify store: [Order](https://shopify.dev/docs/api/admin-rest/2024-01/resources/order), 
[Product](https://shopify.dev/docs/api/admin-rest/2024-01/resources/product),
[Product image](https://shopify.dev/docs/api/admin-rest/2024-01/resources/product-image),
[Product variant](https://shopify.dev/docs/api/admin-rest/2024-01/resources/product-variant),
[Inventory level](https://shopify.dev/docs/api/admin-rest/2024-01/resources/inventorylevel),
[Order](https://shopify.dev/docs/api/admin-rest/2024-01/resources/order),
[Customer](https://shopify.dev/docs/api/admin-rest/2024-01/resources/customer),
[Customer address](https://shopify.dev/docs/api/admin-rest/2024-01/resources/customer-address).



To enable this application, you need to:
 
- [enable custom app development](https://help.shopify.com/en/manual/apps/app-types/custom-apps) for your store. 
- Create a custom app and install it in your store.
- Enable `Write access` Admin API access scope for the following objects:
    - `Orders`
    - `Products`
    - `Inventory`
    - `Customers`
- Obtain the API access token under the API credentials tab of your app.
    
    

Additional documentation is available [here](https://bitbucket.org/kds_consulting_team/kds-team.wr-shopify/src/master/README.md)