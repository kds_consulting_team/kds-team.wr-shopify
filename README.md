# Shopify Data Destination


**Credits:** Client part of this application is partially inspired by 
[Singer.IO shopify TAP project](https://github.com/singer-io/tap-shopify)

**Table of contents:**  
  
[TOC]


# Shopify setup - Prerequisites

To enable this application, you need to:
 
- [Enable custom app development](https://help.shopify.com/en/manual/apps/app-types/custom-apps) for your store. 
- Create a custom app and install it in your store.
- Enable `Write access` Admin API access scope for the following objects:
    - `Orders`
    - `Products`
    - `Inventory`
    - `Customers`
- Obtain the API access token under your app's API credentials tab.
    
    
 
# Configuration

## Authentication 

Make sure you have created a custom application with appropriate permissions.

### Admin API access token

Admin API access token of your private app.

### Shop name

Your shop ID found in the URL, e.g., `[shop_id]`.myshopify.com


## Setup

- Each configuration row accepts a single table on the input. It will be then converted into a JSON model that is required by 
the specified endpoint.
- Each column in the table translates to a JSON property. In some cases, you may include a full JSON string as the column value. 
- Objects may be flattened into columns using the `__` separator. E.g., `customer__id` leads to `{"customer":{"id":"VAL"}}`


**NOTE:** It is possible to include one extra column that serves as a reference to your source data row and is then excluded from the payload.  
You can set this column by setting the `source_id` parameter.




## Supported Endpoints

If you need additional endpoints, please submit your request to
[ideas.keboola.com](https://ideas.keboola.com/) or submit a pull request.



### Products

The Product resource lets you update and create products in a merchant's store. 
You can use [product variants](https://shopify.dev/docs/admin-api/rest/reference/products/product-variant) with the Product resource to create or update 
different versions of the same product. You can also add or update [product images](https://shopify.dev/docs/admin-api/rest/reference/products/product-image).

The expected model is [described here](https://shopify.dev/api/admin-rest/2021-10/resources/product#resource_object):

The input table needs to reflect the property names. All fields are values in the appropriate column, apart from several exceptions:

- `options` -- You can define up to 3 options for each product. Each option has a name and value, which is an array. The column name is formed 
like `option__{OPTION_NAME}` where `{OPTION_NAME}` is the name of the required option. For instance, to create an option `Color`, you should include a column named 
`option__Color` and the value would be a list of option values (note that the order of columns sets the order of options) e.g., 
```
[
             "Pink",
             "Red",
             "Green",
             "Black"]
```
- `variants` --- In this column, you may include a full [product variants](https://shopify.dev/docs/admin-api/rest/reference/products/product-variant) array as a string.
- `images` --- In this column, you may include a full [product images](https://shopify.dev/docs/admin-api/rest/reference/products/product-image) array as a string.

#### Full example

The following input table:

```
"body_html","handle","images","options__Size","product_type","published_at","published_scope","status","tags","template_suffix","title","variants","vendor","options__Color",innerid
"It's the small iPod with a big idea: Video.","ipod-nano","[{
            ""src"": ""http://example.com/burton.jpg""
        }
    ]","[""155"", ""159""]","Cult Products","2007-12-31T19:00:00-05:00","global","active","Emotive, Flash Memory, MP3, Music","special","IPod Nano - 8GB","[{

            ""sku"": ""IPOD2008PINK"",
            ""grams"": 567,
            ""price"": 199.99,
            ""title"": ""Pink"",
            ""weight"": 0.2,
            ""barcode"": ""1234_pink"",
            ""option2"": ""Pink"",
            ""option1"": ""155"",
            ""taxable"": true,
            ""product_id"": 632910392,
            ""weight_unit"": ""kg"",
            ""inventory_policy"": ""continue"",
            ""requires_shipping"": true,
            ""fulfillment_service"": ""manual"",
            ""inventory_management"": ""shopify""
        }
    ]","Apple","[
                ""Pink"",
                ""Red"",
                ""Green"",
                ""Black""]",111

```

Leads to the following object:

```json
{
    "body_html": "It's the small iPod with a big idea: Video.",
    "handle": "ipod-nano",
    "images": [{
        "src": "http://example.com/burton.jpg"
    }
    ],
    "options": [
        {"name": "Color",
         "values": [
             "Pink",
             "Red",
             "Green",
             "Black"]
         },
        {"name": "Size", "values": ["155", "159"]}
    ],
    "product_type": "Cult Products",
    "published_at": "2007-12-31T19:00:00-05:00",
    "published_scope": "global",
    "status": "active",
    "tags": "Emotive, Flash Memory, MP3, Music",
    "template_suffix": "special",
    "title": "IPod Nano - 8GB",
    "variants": [{

        "sku": "IPOD2008PINK",
        "grams": 567,
        "price": 199.99,
        "title": "Pink",
        "weight": 0.2,
        "barcode": "1234_pink",
        "option1": "Pink",
        "taxable": true,
        "weight_unit": "kg",
        "inventory_policy": "continue",
        "requires_shipping": true,
        "fulfillment_service": "manual",
        "inventory_management": "shopify"
    }
    ],
    "vendor": "Apple"
}
```

#### Other examples

**Create a new product with the default product variant**

|title   |body_html|vendor                   |product_type|tags|
|--------|---------|-------------------------|------------|----|
|Burton Custom Freestyle 151|\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e|Burton|Snowboard|["Barnes \u0026 Noble", "Big Air", "John's Fav"]|


**Create a new unpublished product**

|title   |body_html|vendor                   |product_type|published|
|--------|---------|-------------------------|------------|---------|
|Burton Custom Freestyle 151|\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e|Burton                   |Snowboard   |false    |


**Create a Draft product**

|title   |body_html|vendor                   |product_type|status|
|--------|---------|-------------------------|------------|------|
|Burton Custom Freestyle 151|\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e|Burton                   |Snowboard   |draft |

**Product with multiple variants**

|title   |body_html|vendor                   |product_type|variants|
|--------|---------|-------------------------|------------|--------|
|Burton Custom Freestyle 151|\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e|Burton                   |Snowboard   |[{"option1":"First","price":"10.00","sku":"123"},{"option1":"Second","price":"20.00","sku":"123"}]|

**Create a new product with the default variant and a product image that will be downloaded by Shopify**

|title   |body_html|vendor                   |product_type|images|
|--------|---------|-------------------------|------------|------|
|Burton Custom Freestyle 151|\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e|Burton                   |Snowboard   |[{"src":"http:\\/\\/example.com\\/rails_logo.gif"}]|


**Create a new product with multiple product variants and multiple options**

|title   |body_html|vendor                   |product_type|variants|options__Color   |options__Size |
|--------|---------|-------------------------|------------|--------|-----------------|--------------|
|Burton Custom Freestyle 151|\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e|Burton                   |Snowboard   |[{"option1":"Blue","option2":"155"},{"option1":"Black","option2":"159"}]|["Blue", "Black"]|["155", "159"]|

**Update a product's status**


|id   |status         |
|----------|--------------|
|6180685807765    |draft          |

### Inventory levels

An inventory item represents the physical good available to be shipped to a customer. 
It holds essential information about the physical good, including its SKU and whether its inventory is tracked.

There is a 1:1 relationship between a product variant and an inventory item. Each product variant includes the ID of its related inventory item. You can use the inventory item ID to query the InventoryLevel resource to retrieve inventory information.

This allows you to update the inventory levels for particular Product variants. [More info here](https://shopify.dev/api/admin-rest/2021-10/resources/inventorylevel#top)

#### Setup

To update an inventory level, you will need to know the particular `Inventory Item ID` linked to a Product Variant and its `location ID`. 
This can be retrieved via the Shopify data destination connector, by joining the table `product` with `product_variant` on `product_id` and `product_variant` with `inventory_levels` on `inventory_item_id`.

#### Examples

**Increase inventory level by 100**

|location_id|inventory_item_id|available_adjustment|
|-----------|-----------------|--------------------|
|6884556842 |12250274365496   |100                 |

To decrease the level, just use the negative sign in the available adjustment column: `-100`


**Set inventory level to 100**

|location_id|inventory_item_id|available|
|-----------|-----------------|--------------------|
|6884556842 |12250274365496   |100                 |


### Product Variants

[A variant](https://shopify.dev/api/admin-rest/2021-10/resources/product-variant#top) can be added to a Product resource to represent one version of a product with several options. 
The Product resource will have a variant for every possible combination of its options. 
Each product can have a maximum of three options and a maximum of 100 variants.

The product variants can be created as a part of a product or separately using this endpoint. 

To update the variant, include the `id` column. 

#### Examples

**Create new variant**

|option1   |price         |
|----------|--------------|
|Yellow    |1.00          |


**Create new variant - Complete**

|barcode   |compare_at_price|created_at               |fulfillment_service|grams|image_id|inventory_item_id|inventory_management|inventory_policy|option__option1|price |product_id|requires_shipping|sku         |taxable|tax_code|updated_at               |weight|weight_unit|
|----------|----------------|-------------------------|-------------------|-----|--------|-----------------|--------------------|----------------|---------------|------|----------|-----------------|------------|-------|--------|-------------------------|------|-----------|
|1234_pink |299.00          |2012-08-24T14:01:47-04:00|manual             |567  |434522  |342916           |shopify             |continue        |Pink           |199.00|632910392 |True             |IPOD2008PINK|True   |DA040000|2012-08-24T14:01:47-04:00|100   |oz         |


### Product Images

Any product may have up to 250 images, and images can be in .png, .gif or .jpg format.

[More info here](https://shopify.dev/api/admin-rest/2021-10/resources/product-image#top)

Images can be also included in the Create Product object.

#### Examples

For a list of available properties, refer [here](https://shopify.dev/api/admin-rest/2021-10/resources/product-image#resource_object).

`product_id` is always required.
`id` containing image_id is required on the update.

**Create a new product image with included image data as an attachment**


|product_id |filename   |attachment         |
|-----------|----------|--------------|
|632910392  |boots.png    |BASE64_ENCODED_IMAGE          |


**Create a new product image using a source URL that will be downloaded by Shopify**

|product_id |src   |
|-----------|----------|
|632910392  |http://example.com/rails_logo.gif  |

**Create a new product image and attach it to product variants**

|product_id |filename   |attachment          |variant_ids          |
|-----------|-----------|--------------------|--------------------|
|632910392  |boots.png  |BASE64_ENCODED_IMAGE|[808950810,457924702]|


**Modify an image; change its position and alt tag content**

|product_id|id            |position                 |alt   |
|----------|--------------|-------------------------|------|
|6180685807765|29038486847637|2                        |new alt tag content|


### Orders

It is possible to create or update orders. The order structure is complex, so it is required to build the assigned arrays, such as line items, 
beforehand and include as JSON strings in the column values.

**NOTE:** Objects may be flattened by `__` separator, e.g. `customer__id` => `{"customer":{"id":"asd"}}`

More information on the Order object is [available here](https://shopify.dev/api/admin-rest/2021-10/resources/order#resource_object).

#### Examples 

**Create a simple order with only a product variant ID and no optional parameters**

|line_items|
|----------|
|[{"variant_id":447654529,"quantity":1}]|


**Create a simple order and fulfill it**

|email|fulfillment_status|fulfillments              |line_items                             |
|-----|------------------|--------------------------|---------------------------------------|
|foo@example.com|fulfilled         |[{"location_id":48752903}]|[{"variant_id":447654529,"quantity":1}]|

**Create a partially paid order with an existing customer**

|line_items                                  |customer     |financial_status|
|--------------------------------------------|-------------|-------|
|[{"quantity":1,"variant_id":42632102084859}]|{"id":"0012"}|pending|
|[{"variant_id":447654529,"quantity":1}]     |207119551    |pending|


**Update the order note**

|id                                     |note     |
|---------------------------------------|---------|
|450789469                              |Customer contacted us about a custom engraving on this iPod|



### Customers

Create or update customers. More info on the object structure is available [here](https://shopify.dev/api/admin-rest/2021-10/resources/customer#resource_object).

#### Examples

**Create new customer**

|first_name                             |last_name|email                        |phone       |verified_email|addresses                                                                                                                                                    |
|---------------------------------------|---------|-----------------------------|------------|--------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
|Steve                                  |Lastnameson|steve.lastnameson@example.com|+15142546011|true          |[{"address1":"123 Oak St","city":"Ottawa","province":"ON","phone":"555-1212","zip":"123 ABC","last_name":"Lastnameson","first_name":"Mother","country":"CA"}]|


**Update new customer**

|id                                     |email    |note                         |
|---------------------------------------|---------|-----------------------------|
|5850085818517                          |changed@email.address.com|Customer is a great guy      |

### Customer Address

Add or update customer addresses. [More info on the object structure](https://shopify.dev/api/admin-rest/2021-10/resources/customer-address#resource_object)

`customer_id` column is required.

#### Examples

**Add address to a customer**

|customer_id                            |address1 |address2                     |city    |company  |first_name|last_name   |phone       |province|country|zip    |name               |province_code|country_code|country_name|
|---------------------------------------|---------|-----------------------------|--------|---------|----------|------------|------------|--------|-------|-------|-------------------|-------------|------------|------------|
|5850085818517                          |1 Rue des Carrieres|Suite 1234                   |Montreal|Fancy Co.|Samuel    |de Champlain|819-555-5555|Quebec  |Canada |G1R 4P5|Samuel de Champlain|QC           |CA          |Canada      |


**Update a customer address**

|id                                     |customer_id|zip                          |
|---------------------------------------|-----------|-----------------------------|
|207119551                              |5850085818517|90210                        |


# Output

A table `result` is outputted that contains a reference to the source_id of the record and the result of the transaction. 
If an error happens, you can see the reason and react to it based on this table.


|timestamp|endpoint_type|source_id                |created_id|status|err_message|
|---------|-------------|-------------------------|----------|------|-----------|
|2021-10-25 10:35:27.250466|product_variant|productvar_1                         |41372919922837|success|           |



# Example JSON configuration

```json
{
  "parameters": {
    "#api_token": "XXXXXX",
    "shop": "testshop-dev",
    "endpoint": "product",
    "mode": "update",
    "source_id_reference": {
      "source_id_col": "id",
      "remove_from_data": false
    },
    "debug": false
  }
}
```


### Parameters

- `#api_token` --- Admin password of your private app
- `shop` --- Your shop ID found in the URL, e.g., `[shop_id]`.myshopify.com
- `endpoint` --- One of the supported object types: `[
        "product",
        "product_image",
        "product_variant",
        "inventory_level",
        "order",
        "customer",
        "customer_address"
      ]`
- `mode` --- Create or update the object. Supported values (`update`, `create`). **NOTE**: `inventory_level` supports only `update`
- `source_id_reference`
    - `source_id_col` --- Column name in the source data to use as a reference for results
    - `remove_from_data` --- (boolean) If true, the source column will be removed from data; otherwise, it will be sent as a part of the payload.
- `debug` --- Set the verbose logging level.


# Development


If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to
your custom path in the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace, and run the component with the following
command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone git@bitbucket.org:kds_consulting_team/kds-team.wr-shopify.git kds-team.wr-shopify
cd kds-team.wr-shopify
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Integration

For information about deployment and integration with Keboola, please refer to the
[deployment section of our developer
documentation](https://developers.keboola.com/extend/component/deployment/)
