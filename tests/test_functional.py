SAMPLE_PRODUCT = {
    "body_html": "It's the small iPod with a big idea: Video.",
    "handle": "ipod-nano",
    "images": [{
        "src": "http://example.com/burton.jpg"
    }
    ],
    "options": [
        {"name": "Color",
         "values": [
             "Pink",
             "Red",
             "Green",
             "Black"]
         },
        {"name": "Size", "values": ["155", "159"]}
    ],
    "product_type": "Cult Products",
    "published_at": "2007-12-31T19:00:00-05:00",
    "published_scope": "global",
    "status": "active",
    "tags": "Emotive, Flash Memory, MP3, Music",
    "template_suffix": "special",
    "title": "IPod Nano - 8GB",
    "variants": [{

        "sku": "IPOD2008PINK",
        "grams": 567,
        "price": 199.99,
        "title": "Pink",
        "weight": 0.2,
        "barcode": "1234_pink",
        "option1": "Pink",
        "taxable": True,
        "weight_unit": "kg",
        "inventory_policy": "continue",
        "requires_shipping": True,
        "fulfillment_service": "manual",
        "inventory_management": "shopify"
    }
    ],
    "vendor": "Apple"
}

SAMPLE_VARIANT = {
    "barcode": "1234_pink",
    "compare_at_price": "299.00",
    "created_at": "2012-08-24T14:01:47-04:00",
    "fulfillment_service": "manual",
    "grams": 567,
    "image_id": 434522,
    "inventory_item_id": 342916,
    "inventory_management": "shopify",
    "inventory_policy": "continue",
    "option": {
        "option1": "Pink"
    },
    "price": "199.00",
    "product_id": 632910392,
    "requires_shipping": True,
    "sku": "IPOD2008PINK",
    "taxable": True,
    "tax_code": "DA040000",
    "updated_at": "2012-08-24T14:01:47-04:00",
    "weight": 100,
    "weight_unit": "oz"
}
